module.exports = {
    entry: {
        main: './main.js'  // 设置webpack打包的入口文件
    },
    module:{
        // 最常用的配置
        rules: [
            {
                // 所有js文件
                test: /\.js$/,
                // 都应用下面配置的脚本进行处理
                use: {
                    loader: 'babel-loader',
                    // loader的配置项
                    options: {
                        // babel指定需要一个叫presets 的配置项来指定配置环境
                        presets: ['@babel/preset-env'],
                        // 因为babel的标准loader没有支持jsx的语法，所以要添加jsx的编译插件
                        plugins: [['@babel/plugin-transform-react-jsx', {pragma: 'createElement'}]]
                    }
                }
            }
        ]
    },
    mode: "development", // 设置编译代码为开发版本，易于调试
    optimization: {
        minimize: false  // 关闭文件压缩
    }
}