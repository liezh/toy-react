```
npm install webpack webpack-cli ## 安装webpack
npx webpack ## 运行webpack打包

## 安装babel 以及常用的运行环境
npm install --save-dev babel-loader @babel/core @babel/preset-env

## 支持jsx的编译能力，添加babel的jsx翻译插件
npm install --save-dev @babel/plugin-transform-jsx
```